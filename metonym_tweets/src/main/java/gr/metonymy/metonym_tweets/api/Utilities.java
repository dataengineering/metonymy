package gr.metonymy.metonym_tweets.api;

import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author ICS - FORTH
 */
public class Utilities {
    
     public String getConfigurationParameter(String parameterName) {
        // to read url, username, password from configuration file
        Properties configFile = new Properties();
        try {
            configFile.load(Utilities.class.getClassLoader().getResourceAsStream("conf.properties"));
            String parameterValue = configFile.getProperty(parameterName);
            return parameterValue;

        } catch (IOException e1) {
        }
        return null;

    }
    
}
