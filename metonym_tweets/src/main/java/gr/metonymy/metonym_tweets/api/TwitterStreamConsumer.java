package gr.metonymy.metonym_tweets.api;

import gr.metonymy.metonym_tweets.model.Tweet;

import com.google.gson.Gson;
import gr.metonymy.metonym_tweets.model.JdbcTweetDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.scribe.builder.*;
import org.scribe.builder.api.*;
import org.scribe.model.*;
import org.scribe.oauth.*;

import gr.metonymy.metonym_tweets.model.Tweet;

public class TwitterStreamConsumer extends Thread {

    public final String TWITTER_APP_KEY = "sMo7izD4vhqA9bulIkeDA";
    public final String TWITTER_APP_KEY_SECRET = "33GNJARDJnGWI8dLGtMLYRoHJo9foC8DDmwqE9IPE";
    public final String TWITTER_ACCESS_TOKEN = "218041539-U0kzmfVi2cj7OKbklom0Tdq5bXGz3XXZvLqPlZXU";
    public final String TWITTER_ACCESS_TOKEN_SECRET = "XRvEDpCGIqePsKBa3qgtmEkgC8VqQXoS9DpzuLFiM";

    private static final String STREAM_URI = "https://stream.twitter.com/1.1/statuses/filter.json?track=twitter";
    private String latestTweet;
    private int tweetCount;

    public String getLatestTweet() {
        return latestTweet;
    }

    public int getTweetCount() {
        return tweetCount;
    }

    public void run() {
        InputStream in = getClass().getResourceAsStream("/" + "keywords.txt");
        // FileReader f = new FileReader("/src/main/resources/"+filename);
        BufferedReader openFile = new BufferedReader(new InputStreamReader(in));

        String track = "";
        try {
            String keyword = openFile.readLine();
            do {
                track = track + keyword + ",";
                keyword = openFile.readLine();
            } while (keyword != null);
            track = track.substring(0, track.length() - 1);

        } catch (IOException ex) {
            Logger.getLogger(TwitterStreamConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        Utilities u = new Utilities();

        String twitter_app_key = TWITTER_APP_KEY;
        String twitter_app_key_secret = TWITTER_APP_KEY_SECRET;

        String twitter_access_token = TWITTER_ACCESS_TOKEN;
        String twitter_access_token_secret = TWITTER_ACCESS_TOKEN_SECRET;
        System.out.println(twitter_app_key);
        try {
            // Enter your consumer key and secret below
            OAuthService service = new ServiceBuilder()
                    .provider(TwitterApi.class)
                    .apiKey(twitter_app_key)
                    .apiSecret(twitter_app_key_secret)
                    .build();

            // Set your access token
            Token accessToken = new Token(twitter_access_token, twitter_access_token_secret);

            // Let's generate the request
            System.out.println("Connecting to Twitter Public Stream");
            OAuthRequest request = new OAuthRequest(Verb.POST, STREAM_URI);
            request.addHeader("version", "HTTP/1.1");
            request.addHeader("host", "stream.twitter.com");
            request.setConnectionKeepAlive(true);
            request.addHeader("user-agent", "Twitter Stream Reader");
         //   request.addBodyParameter("track", track); // Set keywords you'd like to track here
            request.addBodyParameter("lang", "el"); // Set keywords you'd like to track here
            service.signRequest(accessToken, request);
            Response response = request.send();

            // Create a reader to read Twitter's stream
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getStream()));

            String line;
            Gson myGson = new Gson();
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                Tweet tweet = myGson.fromJson(line, Tweet.class);
                // TweetDAO tweetDAO = (TweetDAO) context.getBean("TweetDAO");
                JdbcTweetDAO tweetDAO = new JdbcTweetDAO();
                if (tweet != null) {
                    tweetDAO.insertTweet(tweet);
                    latestTweet = line;
                    tweetCount++;
                }
                // System.out.println(line);
            }
        } catch (IOException ioe) {
        }

    }
}
