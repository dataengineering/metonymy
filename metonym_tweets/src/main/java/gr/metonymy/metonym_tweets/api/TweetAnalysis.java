package gr.metonymy.metonym_tweets.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author katerinapapantoniou
 */
public class TweetAnalysis {

    final TwitterStreamConsumer streamConsumer = new TwitterStreamConsumer();

    private static final long serialVersionUID = 1L;

    public static void main(String args[]) {
        TweetAnalysis t = new TweetAnalysis();
        t.streamConsumer.start();
        System.out.println("Tweet Count: " + Integer.toString(t.streamConsumer.getTweetCount()) + "");
        System.out.println("Tweets: " + t.streamConsumer.getLatestTweet() + "");

    }
}
