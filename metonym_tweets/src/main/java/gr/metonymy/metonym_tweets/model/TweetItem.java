package gr.metonymy.metonym_tweets.model;

/**
 *
 * @author katerinapapantoniou
 */
public class TweetItem {
    private int tweet_id;
    private String tweet_date;
    private String tweet_language;
    private String tweet_country;
    private String tweet_user;
    private String tweet_text;
    private String tweet_type;
    private String tweet_location;

    /**
     * @return the tweet_id
     */
    public int getTweet_id() {
        return tweet_id;
    }

    /**
     * @param tweet_id the tweet_id to set
     */
    public void setTweet_id(int tweet_id) {
        this.tweet_id = tweet_id;
    }

    /**
     * @return the tweet_date
     */
    public String getTweet_date() {
        return tweet_date;
    }

    /**
     * @param tweet_date the tweet_date to set
     */
    public void setTweet_date(String tweet_date) {
        this.tweet_date = tweet_date;
    }

    /**
     * @return the tweet_language
     */
    public String getTweet_language() {
        return tweet_language;
    }

    /**
     * @param tweet_language the tweet_language to set
     */
    public void setTweet_language(String tweet_language) {
        this.tweet_language = tweet_language;
    }

    /**
     * @return the tweet_country
     */
    public String getTweet_country() {
        return tweet_country;
    }

    /**
     * @param tweet_country the tweet_country to set
     */
    public void setTweet_country(String tweet_country) {
        this.tweet_country = tweet_country;
    }

    /**
     * @return the tweeet_user
     */
    public String getTweet_user() {
        return tweet_user;
    }

    /**
     * @param tweeet_user the tweeet_user to set
     */
    public void setTweet_user(String tweet_user) {
        this.tweet_user = tweet_user;
    }

    /**
     * @return the tweet_text
     */
    public String getTweet_text() {
        return tweet_text;
    }

    /**
     * @param tweet_text the tweet_text to set
     */
    public void setTweet_text(String tweet_text) {
        this.tweet_text = tweet_text;
    }

    /**
     * @return the tweet_location
     */
    public String getTweet_location() {
        return tweet_location;
    }

    /**
     * @param tweet_location the tweet_location to set
     */
    public void setTweet_location(String tweet_location) {
        this.tweet_location = tweet_location;
    }

    /**
     * @return the tweet_type
     */
    public String getTweet_type() {
        return tweet_type;
    }

    /**
     * @param tweet_type the tweet_type to set
     */
    public void setTweet_type(String tweet_type) {
        this.tweet_type = tweet_type;
    }

}


