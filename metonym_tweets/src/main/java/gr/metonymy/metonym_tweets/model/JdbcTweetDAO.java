package gr.metonymy.metonym_tweets.model;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

/**
 *
 * @author katerina papantoniou
 */
public class JdbcTweetDAO  {

public Connection createConnection() {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "realestate2", "realestate2");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");

        return c;
    }
   
    public void printTweet(TweetItem item) {
        System.out.println("-----------------------------------");
        System.out.println("Tweet ID" + item.getTweet_id());
        System.out.println("Tweet Type" + item.getTweet_type());
        System.out.println("Tweet Text" + item.getTweet_text());
        System.out.println("Tweet User" + item.getTweet_user());
        System.out.println("Tweet Language" + item.getTweet_language());
        System.out.println("Tweet Date" + item.getTweet_date());
        System.out.println("Tweet Location" + item.getTweet_location());
        System.out.println("-----------------------------------");
    }

    
    public ArrayList<TweetItem> getTweets() {
        String sql = "";
        ArrayList<TweetItem> tweets = new ArrayList<TweetItem>();

        sql = "SELECT tweet_text, tweet_date, tweet_user, tweet_location"
                + " FROM " + " metonymy.tweet " + " ";

        Connection conn = null;

        try {
            conn= createConnection();
            PreparedStatement ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                TweetItem r_obj = new TweetItem();
                r_obj.setTweet_text(rs.getString("tweet_text"));
                r_obj.setTweet_user(rs.getString("tweet_user"));
                r_obj.setTweet_location(rs.getString("tweet_location"));
//                r_obj.(rs.getString("name"));
//                r_obj.setHasSubregion(rs.getInt("hassubregion"));
                tweets.add(r_obj);
                    //r_obj.setIsSubRegionOf(isSubRegionOf);
                //to do more changes
                //  poiObject.setPoi_id(Integer.parseInt(rs.getString(1)));
                //  poiObject.setName(rs.getString(3));

                // feedData.add(feedObject);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }

        return tweets;

    }

    public int insertTweet(Tweet item) {
        int returned_int = -2;
        String query = null;
        Connection conn = null;
        try {
            conn = createConnection();

            // ByteBuffer text = Charset.forName("UTF-8").encode(item.getText());
            if (item.getText() != null) {

                String text = item.getText().replace("'", "");

                System.out.println("Text: " + item.getText());

                query = "INSERT INTO "
                        + "metonymy.tweet"
                        + "(tweet_text,tweet_language,tweet_date,tweet_country,tweet_location,tweet_user,tweet_type)"
                        + " VALUES ('" + text + "','" + item.getLang() + "','" + item.getCreated_at() + "',"
                        + "" + null
                        + "," + null
                        + "," + null
                        + ",'" + item.getSource()
                        + "'"
                        + ""
                        + ")";

                PreparedStatement ps = conn.prepareStatement(query);
                returned_int = ps.executeUpdate();
                ps.close();
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }
        return returned_int;

    }
}
