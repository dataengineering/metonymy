﻿﻿-- +---------------------------------------------------------
-- | MODEL       : Metonymy Tweets Logical Model
-- | DATE        : 28 - 4 - 2018
-- | UPDATED     : 28 - 4 - 2018
-- +---------------------------------------------------------
-- | WARNING     : Review before execution
-- +---------------------------------------------------------
 
ALTER SEQUENCE metomymy.tweet_tweet_id_seq RESTART WITH 1;

-- +---------------------------------------------------------
-- | TRUNCATE
-- +---------------------------------------------------------
  
TRUNCATE metonymy.tweet CASCADE;


 
-- +---------------------------------------------------------
-- | DROP
-- +---------------------------------------------------------
 
DROP TABLE IF EXISTS  metonymy.tweet CASCADE;
DROP SCHEMA IF EXISTS metonymy CASCADE;
DROP DATABASE IF EXISTS metonymy;
 
-- +---------------------------------------------------------
-- | CREATE DATABASE
-- +---------------------------------------------------------
CREATE DATABASE metonymy;
 
-- +---------------------------------------------------------
-- | CREATE SCHEMA
-- +---------------------------------------------------------
CREATE SCHEMA metonymy;
 
-- +---------------------------------------------------------
-- | CREATE TABLE
-- +---------------------------------------------------------
CREATE TABLE metonymy.tweet (
    tweet_id SERIAL, --primary index automatically
    tweet_date text,
    tweet_language text,
    tweet_country text, 
    tweet_user text,
    tweet_text text,
    tweet_type text,
    tweet_location text,
    PRIMARY KEY (tweet_id)
);
 
  
